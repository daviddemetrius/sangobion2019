<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <?php include('metatag.php');?>
  <title>Home | SANGOBION</title>
  <link rel="icon" href="img/favicon.ico">
  <?php include('stylesheet.php');?>
</head>

<body>
  <main class="main-wrap" id="search-result">
    <?php include('header.php');?>
    <!-- body start -->
    <section class="section">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="search-totalinfo">
              <span>Search result for:</span> Blood(260)
            </div>
            <div class="main-slider wow fadeInUp" data-wow-delay="0.25s">
              <div class="item-slider">
                <div class="item-inside faq">
                  <div class="row">
                    <div class="col-12">
                      <div class="search-wrapper">
                        <div class="search-item">
                          <div class="text">
                            <div class="title">
                              Blood Health Expert
                            </div>
                            <div class="short-desc">
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed ullamcorper ipsum, eget rutrum lacus. Nunc rutrum molestie lacinia. Integer venenatis nunc nec quam pellentesque iaculis.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed ullamcorper ipsum, eget rutrum lacus. Nunc rutrum molestie lacinia. Integer venenatis nunc nec quam pellentesque iaculis.</p>
                            </div>
                            <div class="link">
                              <a href="#" class="link-color">Read More</a>
                            </div>
                          </div>
                        </div>
                        <div class="search-item">
                          <div class="text">
                            <div class="title">
                              Blood Health Expert
                            </div>
                            <div class="short-desc">
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed ullamcorper ipsum, eget rutrum lacus. Nunc rutrum molestie lacinia. Integer venenatis nunc nec quam pellentesque iaculis.</p>
                            </div>
                            <div class="link">
                              <a href="#" class="link-color">Read More</a>
                            </div>
                          </div>
                        </div>
                        <div class="search-item">
                          <div class="text">
                            <div class="title">
                              Blood Health Expert
                            </div>
                            <div class="short-desc">
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed ullamcorper ipsum, eget rutrum lacus. Nunc rutrum molestie lacinia. Integer venenatis nunc nec quam pellentesque iaculis.</p>
                            </div>
                            <div class="link">
                              <a href="#" class="link-color">Read More</a>
                            </div>
                          </div>
                        </div>
                        <div class="search-item">
                          <div class="text">
                            <div class="title">
                              Blood Health Expert
                            </div>
                            <div class="short-desc">
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed ullamcorper ipsum, eget rutrum lacus. Nunc rutrum molestie lacinia. Integer venenatis nunc nec quam pellentesque iaculis.</p>
                            </div>
                            <div class="link">
                              <a href="#" class="link-color">Read More</a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="tab-info">
                        <div class="text">
                          <p>Can’t find what you’re looking for? Check out other’s Q&A with our expert</p>
                        </div>
                        <div class="link">
                          <a href="contactus.php" data-slide="2" class="btn btn-red">Contact Us</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-12">

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php include('footer.php');?>
  </main>
  <?php include('script.php');?>
</body>
</html>

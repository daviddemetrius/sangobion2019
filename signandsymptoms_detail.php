<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <?php include('metatag.php');?>
  <title>Home | SANGOBION</title>
  <link rel="icon" href="img/favicon.ico">
  <?php include('stylesheet.php');?>
</head>

<body>
  <main class="main-wrap" id="article-details">
    <?php $page = 'signandsymptoms';include('header.php');?>
    <!-- body start -->
    <section class="section main-detail">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="detail-wrapper">
              <div class="row justify-content-center">
                <div class="col-12">
                  <div class="share-socialmedia wow fadeInDown d-none">
                    <ul class="list-inline">
                      <li class="list-inline-item"><label>Share This :</label></li>
                      <li class="list-inline-item"><a href="" class="share-link facebook"></a></li>
                      <li class="list-inline-item"><a href="" class="share-link instagram"></a></li>
                      <li class="list-inline-item"><a href="" class="share-link twitter"></a></li>
                      <li class="list-inline-item"><a href="" class="share-link googleplus"></a></li>
                    </ul>
                  </div>
                  <div class="detail-main wow fadeInUp" data-wow-delay="0.25s">
                    <div class="title">
                      Not sure if you matched the symptoms? Talk to our doctors!
                    </div>
                    <div class="row">
                      <div class="col-lg-6 col-12">
                        <div class="image">
                          <img src="img/signandsymptoms_detail_1.png">
                        </div>
                      </div>
                      <div class="col-lg-6 col-12">
                        <div class="desc">
                          <p>As the name implies, iron deficiency anemia is due to lack of or insufficient iron. Without adequate amount of iron, your body wouldn’t be able to produce enough hemoglobin in red blood cells to carry oxygen throughout the body.</p>
                          <p>As a result, iron deficiency anaemia may often leave you tired and short of breath.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-12">
                  <div class="content wow fadeInUp" data-wow-delay="0.25s">
                    <div class="desc">
                      <p>IDA can be cause by a lot of factors, such as blood loss, low intake of iron, and poor absorption of iron from diets. There are also stages in life when iron requirements are especially high (i.e. growth and pregnancy). Hence, 3 population groups that are very prevalent to IDA are young children, women in general, and pregnant women.</p>
                      <p>In cases of young children, they need extra iron during growth spurts. If the children isn’t eating a healthy, rich-iron diet, he or she may be at risk of anemia. For women in general, they lose blood during menstruation, which also puts them in greater risk of IDA. Pregnant women should also monitor their iron level since they need to serve their own increased blood volume as well as be a source of hemoglobin for the growing fetus.</p>
                    </div>
                  </div>
                </div>
                <div class="col-lg-10 col-md-11 col-12">
                  <div class="image wow fadeInUp" data-wow-delay="0.25s">
                    <img src="img/signandsymptoms_detail_2.png">
                  </div>
                  <div class="content wow fadeInUp" data-wow-delay="0.25s">
                    <div class="subtitle">
                      Conclusion
                    </div>
                    <div class="desc">
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="section slider-overflow">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section-heading wow fadeInDown">
              <div class="heading">
                Recommended Products
              </div>
            </div>
            <div class="recommended-slider wow fadeInUp" data-wow-delay="0.25s">
              <div class="recommended-col">
                <div class="recommended-item">
                  <div class="text">
                    <div class="title">Product for <strong>Adults</strong></div>
                  </div>
                  <div class="image">
                    <img src="img/img-5.png" alt="sangobion fit">
                  </div>
                  <div class="link">
                    <a href="#">						
                      <div class="btn-learn-more">View Product</div>
                      <div class="icon-arrow-down"></div>						
                    </a>
                  </div>
                </div>
              </div>
              <div class="recommended-col">
                <div class="recommended-item">
                  <div class="text">
                    <div class="title">Product for <strong>Adults</strong></div>
                  </div>
                  <div class="image">
                    <img src="img/img-5.png" alt="sangobion fit">
                  </div>
                  <div class="link">
                    <a href="#">						
                      <div class="btn-learn-more">View Product</div>
                      <div class="icon-arrow-down"></div>						
                    </a>
                  </div>
                </div>
              </div>
              <div class="recommended-col">
                <div class="recommended-item">
                  <div class="text">
                    <div class="title">Product for <strong>Adults</strong></div>
                  </div>
                  <div class="image">
                    <img src="img/img-5.png" alt="sangobion fit">
                  </div>
                  <div class="link">
                    <a href="#">						
                      <div class="btn-learn-more">View Product</div>
                      <div class="icon-arrow-down"></div>						
                    </a>
                  </div>
                </div>
              </div>
              <div class="recommended-col">
                <div class="recommended-item">
                  <div class="text">
                    <div class="title">Product for <strong>Adults</strong></div>
                  </div>
                  <div class="image">
                    <img src="img/img-5.png" alt="sangobion fit">
                  </div>
                  <div class="link">
                    <a href="#">						
                      <div class="btn-learn-more">View Product</div>
                      <div class="icon-arrow-down"></div>						
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php include('footer.php');?>
  </main>
  <?php include('script.php');?>
</body>
</html>

$(document).ready(function() {
    new WOW().init();
	$('#layerslider').layerSlider({
		sliderVersion: '6.0.0',
		responsiveUnder: 0,
		layersContainer: 0,
		hoverPrevNext: false,
		navStartStop: false,
		navButtons: false,
		firstSlide: 'random',
		skin: 'v5',
		skinsPath: 'js/layerslider/skins/'
	});
	$('#layerslider2').layerSlider({
		sliderVersion: '6.0.0',
		responsiveUnder: 0,
		layersContainer: 0,
		hoverPrevNext: false,
		navStartStop: false,
		navButtons: false,
		skin: 'v5',
		skinsPath: 'js/layerslider/skins/'
	});
	$('#layerslider3').layerSlider({
		type: 'responsive',
		sliderVersion: '6.0.0',
		responsiveUnder: 0,
		layersContainer: 0,
		hoverPrevNext: false,
		navStartStop: false,
		navButtons: false,
		showCircleTimer: false,
		skin: 'v5',
		skinsPath: 'js/layerslider/skins/'
	});
	$('#layerslider_mobile').layerSlider({
		sliderVersion: '6.0.0',
		responsiveUnder: 0,
		layersContainer: 0,
		hoverPrevNext: false,
		navStartStop: false,
		navButtons: false,
		firstSlide: 'random',
		skin: 'v5',
		skinsPath: 'js/layerslider/skins/'
	});
	
	//dom
	$('[data-fancybox]').fancybox();
	
	//hover
	$('.owl-filter').hover(
		function(){ $(this).addClass('active') },
		function(){ $(this).removeClass('active')}
	)
	
	//select jump
	$('#select-region').on('change', function() {  
		var url = this.value;  
		window.open(url, '_blank');
	});
});

$(window).scroll(function () {
    var sc = $(window).scrollTop()
    if (sc > 50) {
        $(".header").addClass("small")
    } else {
        $(".header").removeClass("small")
    }
});

//carousel
$(document).ready(function() {
	"use strict";	
        
        $(function() {
        var owl = $('.filter-item').owlCarousel({
            loop    :true,
            // center: true,
            margin  :30,
            nav     :true,
            navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
            responsive:{
                0:{
                    items:1,
                    loop: false
                },
                768:{
                    items:3
                },
                992:{
                    items:3
                },
                1200:{
                    items:4
                }
            }
        }); 
        
        /* animate filter */
        var owlAnimateFilter = function(even) {
            $(this)
            .addClass('__loading')
            .delay(70 * $(this).parent().index())
            .queue(function() {
                $(this).dequeue().removeClass('__loading')
            })
        };

        $('.filter-menu').on('click', '.filter-btn', function(e) {
            var filter_data = $(this).data('filter');

            /* return if current */
            if($(this).hasClass('btn-active')) return;

            /* active current */
            $(this).addClass('btn-active').siblings().removeClass('btn-active');

            /* Filter */
            owl.owlFilter(filter_data, function(_owl) { 
                $(_owl).find('.item').each(owlAnimateFilter); 
            });
        });
    })
});


//select
console.clear();

var el = {};

$('.placeholder').on('click', function (ev) {
  $('.placeholder').css('opacity', '0');
  $('.list__ul').toggle();
});

 $('.list__ul a').on('click', function (ev) {
   ev.preventDefault();
   var index = $(this).parent().index();
   
   $('.placeholder').text( $(this).text() ).css('opacity', '1');
   
   console.log($('.list__ul').find('li').eq(index).html());
   
   $('.list__ul').find('li').eq(index).prependTo('.list__ul');
   $('.list__ul').toggle();   
   
 });


$('select').on('change', function (e) {
  
  // Set text on placeholder hidden element
  $('.placeholder').text(this.value);
  
  // Animate select width as placeholder
  $(this).animate({width: $('.placeholder').width() + 'px' });
  
});

$(document).ready(function(){
    /*  -- ADDING JS  */
    $('.btn-search').on('click', function(){
        if($('#search').hasClass('hidden')) {
            $(this).parents().find('.globalsearch-wrapper').addClass('show');
            $('#search').removeClass('hidden');
        }
        else {
            $('#search').addClass('hidden');
            $(this).parents().find('.globalsearch-wrapper').removeClass('show');
        }
    });
    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();
    
        if (scroll >= 30) {
            $("header").addClass("scrolled");
        } else {
            $("header").removeClass("scrolled");
        }
    });
    $('.main-slider').slick({
        slidesToShow: 1,
        infinite: false,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.main-tabs',
        centerMode: true,
        focusOnSelect: true
    });
    $('.main-tabs').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        asNavFor: '.main-slider',
        dots: false,
        centerMode: true,
        focusOnSelect: true
    });
    $('a[data-slide]').click(function(e) {
        e.preventDefault();
        var slideno = $(this).data('slide');
        $('.main-tabs').slick('slickGoTo', slideno - 1);
      });
    $('.recommended-slider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        responsive : [
            { breakpoint: 1199, settings: {slidesToShow: 3} },
            { breakpoint: 991,  settings: {slidesToShow: 2} },
            { breakpoint: 767,  settings: {slidesToShow: 2, arrows:true} }
        ]
    });
    var $carousel = $('.product-slider').slick({
        autoplay : true,
        autoplaySpeed : 1000,
        slidesToShow : 4,
        slidesToScroll : 1,
        infinite: false,
        arrows: true,
        responsive : [
            { breakpoint: 1199, settings: {slidesToShow: 4} },
            { breakpoint: 991,  settings: {slidesToShow: 3} },
            { breakpoint: 767,  settings: {slidesToShow: 1,variableWidth: true,centerMode:true} }
    ]
    });
    $carousel.on('beforeChange', function(event, slick, currentSlide, nextSlide){
        // alert('asdf');
        $carousel.find('.slick-slide.slick-active').addClass('clicked');
    });
    $carousel.on('afterChange', function(event, slick, currentSlide, nextSlide){
        $carousel.find('.slick-slide.slick-active').addClass('clicked');
    });
    $('.slick-slide').each(function(){
        var attr = $(this).children().find('.slide').attr('class');
        $(this).addClass(attr);
    });
    $('.slick-slide.slick-active').each(function(){
        $(this).addClass('clicked');
    });
    $('.item-tab').each(function(){
        if($(this).attr('data-target') == 'all') {
            $(this).addClass('active');
        };
    });
    $('.item-tab').click(function(e) {
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
        var attr = $(this).attr('data-target');
        if(attr !== 'all') {
            $carousel.slick('slickUnfilter');
            $carousel.slick('slickFilter', '.' + attr + '');
        }
        else {
            $carousel.slick('slickUnfilter');
            $carousel.slick('slickFilter', '.all');
        }
    });
    $('.slide.slick-active').click(function(){
        $(this).siblings().removeClass('active-fancybox');
        $(this).addClass('active-fancybox');
        $('.slide').addClass('blur-image');
    });
    $('[data-fancybox]').fancybox({
        afterClose  : function() {
            $('.slide').removeClass('blur-image','active-fancybox');
        }
    });

    $campaign_main = $('.campaign-slider').slick({
        // autoplay : true,
        // autoplaySpeed : 1000,
        slidesToShow : 1,
        slidesToScroll : 1,
        infinite: true,
        arrows: true,
        responsive : [
            { breakpoint: 991, settings: {arrows: false} }
        ]
    });
    $campaign_nav = $('.campaign-nav').slick({
        // autoplay : true,
        // autoplaySpeed : 1000,
        slidesToShow : 7,
        slidesToScroll : 1,
        infinite: true,
        arrows: true,
        responsive : [
            { breakpoint: 1199, settings: {slidesToShow: 6} },
            { breakpoint: 991,  settings: {slidesToShow: 5} },
            { breakpoint: 767,  settings: {slidesToShow: 4} }
        ]
    });
    $('.item-nav').click(function() {
        var src_img = $(this).children().attr('src');
        
        $(this).parents('.campaign-nav').siblings('.image').children('img').fadeOut(function(){
            $(this).attr('src',src_img).fadeIn();
        })
    });
    $('.slick-slide').each(function(){
        var attr = $(this).children().find('.slide').attr('class');
        $(this).addClass(attr);
    });
    $('.slick-slide').each(function(){
        var attr = $(this).children().find('.item-nav').attr('class');
        $(this).addClass(attr);
    });
    $('.campaign-tab').each(function(){
        if($(this).attr('data-target') == 'all') {
            $(this).addClass('active');
        };
    });
    $('.campaign-tab').click(function(e) {
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
        var attr = $(this).attr('data-target');
        if(attr !== 'all') {
            $campaign_main.slick('slickUnfilter');
            $campaign_main.slick('slickFilter', '.' + attr + '');
            $campaign_nav.slick('slickUnfilter');
            $campaign_nav.slick('slickFilter', '.' + attr + '');
        }
        else {
            $campaign_main.slick('slickUnfilter');
            $campaign_main.slick('slickFilter', '.all');
            $campaign_nav.slick('slickUnfilter');
            $campaign_nav.slick('slickFilter', '.all');

        }
    });
    $(function(){
        // bind change event to select
        $('.selection .custom-select').on('change', function () {
            var url = $(this).val(); // get selected value
            if (url) { // require a URL
                window.location = url; // redirect
            }
            return false;
        });
      });
    $(document).on('click', '.slick-cloned', function(e) {
    var $slides = $(this)
    .parent()
    .children('.slick-slide:not(.slick-cloned)');

    $slides
    .eq( ( $(this).attr("data-slick-index") || 0) % $slides.length )
    .trigger("click.fb-start", { $trigger: $(this) });

    return false;
    });
    $('.maps')
	.click(function(){
			$(this).find('iframe').addClass('clicked')})
	.mouseleave(function(){
            $(this).find('iframe').removeClass('clicked')});
            
    $(function() {
        $('.globalsearch-wrapper form').each(function() {
            $(this).find('input[name="search"]').keypress(function(e) {
                // Enter pressed?
                if(e.which == 10 || e.which == 13) {
                    // alert('asdfasdf');
                    // this.form.submit();
                    window.location.href='search-result.php';
                }
            });
    
            // $(this).find('input[type=submit]').hide();
        });
    });
});
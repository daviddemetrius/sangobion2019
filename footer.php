<!-- footer start -->
<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-12 align-self-end">
				<div class="logo">
					<img src="img/logo-sangobion-w-drop-white.png">
				</div>
			</div>
			<div class="col-lg-7 col-12 d-none d-md-block">
				<div class="footer-menu">
					<div class="row">
						<div class="col-md-4 col-12">
							<div class="heading">
								Signs & Symptoms :
							</div>
							<ul class="list-unstyled">
								<li><a href="signandsymptoms.php">Spot the Sign and Symptoms</a></li>
								<li><a href="">Anemia Meter</a></li>
								<li><a href="">Health Consequences of Anemia</a></li>
							</ul>
						</div>
						<div class="col-md-4 col-12">
							<div class="heading">
								Learn About :
							</div>
							<ul class="list-unstyled">
								<li><a href="article-a.php">Anemia</a></li>
								<li><a href="article-b.php">Iron Deficiency</a></li>
								<li><a href="article-d.php">Menstrual Pain</a></li>
								<li><a href="article-c.php">Performance</a></li>
							</ul>
						</div>
						<div class="col-md-4 col-12">
							<ul class="list-unstyled no-heading">
								<li><a href="blood-health-expert.php">Blood Health Expert</a></li>
								<li><a href="product.php">Products</a></li>
								<li><a href="campaign.php">Campaign</a></li>
								<li><a href="contactus.php">Contact Us</a></li>
								<li><a href="">Privacy Statement</a></li>
								<li><a href="">Term & Condition</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 d-block d-md-none">
				<div class="footer-menu">
					<div class="row">
						<div class="col-md-4 col-12">
							<ul class="list-unstyled no-heading">
								<li><a href="">Privacy Statement</a></li>
								<li><a href="">Term & Condition</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12">
				<div class="row">
					<div class="col-lg-6 col-12">
						<div class="language">
							<div class="btn-group dropup">
								<button type="button" class="btn btn-label">
									Select Country : 
								</button>
								<button type="button" class="btn btn-result dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<img src="img/flag-indonesia2.png"> Indonesia
								</button>
								<div class="dropdown-menu">
									<!-- Dropdown menu links -->
									<a class="dropdown-item" href="#">Indonesia</a>
									<a class="dropdown-item" href="#">Thailand</a>
									<a class="dropdown-item" href="#">Singapore</a>
									<a class="dropdown-item" href="#">Malaysia</a>
									<a class="dropdown-item" href="#">Philippine</a>
									<a class="dropdown-item" href="#">Vietnam</a>
									<a class="dropdown-item" href="#">India</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-12">
						<div class="socialmedia">
							<ul class="list-inline">
								<li class="list-inline-item"><a href="" class="socialmedia-item facebook"></a></li>
								<li class="list-inline-item"><a href="" class="socialmedia-item instagram"></a></li>
								<li class="list-inline-item"><a href="" class="socialmedia-item twitter"></a></li>
								<li class="list-inline-item"><a href="" class="socialmedia-item googleplus"></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="copyrights">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-4 col-12 pg-footer order-lg-2">
					<a href="https://www.pg.com/id_ID/" target="_blank"><img src="img/footer-logo-png-health.png" alt="logo P&amp;G" height="36px"></a>
				</div>
				<div class="col-lg-8 col-12 order-lg-1">
					<p>© The Procter & Gamble Company and affiliates, 2019 These products are marketed by The Procter & Gamble Company and its affiliates and are not associated with Merck & Co.</p>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- footer end -->
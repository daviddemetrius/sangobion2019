<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <?php include('metatag.php');?>
  <title>Home | SANGOBION</title>
  <link rel="icon" href="img/favicon.ico">
  <?php include('stylesheet.php');?>
</head>

<body>
  <?php $page = 'product';include('header.php');?>
  <main class="main-wrap" id="product">
    <!-- body start -->
    <section class="section">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="product-preview">
              <div class="section-heading">
                <div class="heading with-desc wow fadeInDown">
                  Explore our range
                </div>
                <div class="desc wow fadeInDown" data-wow-delay="0.25s">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.
                </div>
              </div>
              <div class="category-tabs d-none wow fadeInUp" data-wow-delay="0.5s">
                <div class="item-tab" data-target="all">
                  All
                </div>
                <div class="item-tab" data-target="work-productivity">
                  Work Productivity
                </div>
                <div class="item-tab" data-target="cognition">
                  Cognition
                </div>
                <div class="item-tab" data-target="growth">
                  Growth
                </div>
                <div class="item-tab" data-target="immunity">
                  Immunity
                </div>
              </div>
              <div class="product-slider wow fadeInUp" data-wow-delay="0.5s">
                <a class="slide all growth" href="#growth-1" data-fancybox="growth">
                  <img src="img/products_1.png">
                </a>
                <a class="slide all work-productivity" href="#work-productivity-1" data-fancybox="work-productivity">
                  <img src="img/products_2.png">
                </a>
                <a class="slide all immunity" href="#immunity-1" data-fancybox="immunity">
                  <img src="img/products_3.png">
                </a>
                <a class="slide all cognition" href="#cognition-1" data-fancybox="cognition">
                  <img src="img/products_4.png">
                </a>
                <a class="slide all cognition" href="#growth-2" data-fancybox="growth">
                  <img src="img/products_5.png">
                </a>
                <a class="slide all cognition" href="#work-productivity-2" data-fancybox="work-productivity">
                  <img src="img/products_6.png">
                </a>
                <a class="slide all cognition" href="#immunity-2" data-fancybox="immunity">
                  <img src="img/products_7.png">
                </a>
              </div>
              <div class="product-detail" style="display: none;" id="work-productivity-1">
                <div class="container">
                  <div class="row">
                    <div class="col-xl-7 col-lg-8 col-md-10 col-12">
                      <div class="image">
                        <img src="img/products_2.png">
                      </div>
                    </div>
                    <div class="col-lg-10 col-12">
                      <div class="share-socialmedia">
                        <ul class="list-inline">
                          <li class="list-inline-item"><label>Share :</label></li>
                          <li class="list-inline-item"><a href="" class="share-link facebook"></a></li>
                          <li class="list-inline-item"><a href="" class="share-link instagram"></a></li>
                          <li class="list-inline-item"><a href="" class="share-link twitter"></a></li>
                          <li class="list-inline-item"><a href="" class="share-link googleplus"></a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-lg-10 col-12">
                      <div class="content">
                        <div class="row">
                          <div class="col-12">
                            <div class="title">
                              Sangobion Fizz
                            </div>
                          </div>
                          <div class="col-lg-4 col-12">
                            <div class="category">
                              <ul class="list-unstyled">
                                <li>Work Productivity</li>
                                <li>Cognition</li>
                              </ul>
                            </div>
                          </div>
                          <div class="col-lg-8 col-12">
                            <div class="desc">
                              <ul>
                              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                              <li>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</li>
                              <li>Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </li>
                              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="product-detail" style="display: none;" id="growth-1">
                <div class="container">
                  <div class="row">
                    <div class="col-xl-7 col-lg-8 col-md-10 col-12">
                      <div class="image">
                        <img src="img/products_1.png">
                      </div>
                    </div>
                    <div class="col-lg-10 col-12">
                      <div class="share-socialmedia">
                        <ul class="list-inline">
                          <li class="list-inline-item"><label>Share :</label></li>
                          <li class="list-inline-item"><a href="" class="share-link facebook"></a></li>
                          <li class="list-inline-item"><a href="" class="share-link instagram"></a></li>
                          <li class="list-inline-item"><a href="" class="share-link twitter"></a></li>
                          <li class="list-inline-item"><a href="" class="share-link googleplus"></a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-lg-10 col-12">
                      <div class="content">
                        <div class="row">
                          <div class="col-12">
                            <div class="title">
                              Sangobion Fizz
                            </div>
                          </div>
                          <div class="col-lg-4 col-12">
                            <div class="category">
                              <ul class="list-unstyled">
                                <li>Work Productivity</li>
                                <li>Cognition</li>
                              </ul>
                            </div>
                          </div>
                          <div class="col-lg-8 col-12">
                            <div class="desc">
                              <ul>
                              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                              <li>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</li>
                              <li>Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </li>
                              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="product-detail" style="display: none;" id="immunity-1">
                <div class="container">
                  <div class="row">
                    <div class="col-xl-7 col-lg-8 col-md-10 col-12">
                      <div class="image">
                        <img src="img/products_3.png">
                      </div>
                    </div>
                    <div class="col-lg-10 col-12">
                      <div class="share-socialmedia">
                        <ul class="list-inline">
                          <li class="list-inline-item"><label>Share :</label></li>
                          <li class="list-inline-item"><a href="" class="share-link facebook"></a></li>
                          <li class="list-inline-item"><a href="" class="share-link instagram"></a></li>
                          <li class="list-inline-item"><a href="" class="share-link twitter"></a></li>
                          <li class="list-inline-item"><a href="" class="share-link googleplus"></a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-lg-10 col-12">
                      <div class="content">
                        <div class="row">
                          <div class="col-12">
                            <div class="title">
                              Sangobion Fizz
                            </div>
                          </div>
                          <div class="col-lg-4 col-12">
                            <div class="category">
                              <ul class="list-unstyled">
                                <li>Work Productivity</li>
                                <li>Cognition</li>
                              </ul>
                            </div>
                          </div>
                          <div class="col-lg-8 col-12">
                            <div class="desc">
                              <ul>
                              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                              <li>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</li>
                              <li>Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </li>
                              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="product-detail" style="display: none;" id="cognition-1">
                <div class="container">
                  <div class="row">
                    <div class="col-xl-7 col-lg-8 col-md-10 col-12">
                      <div class="image">
                        <img src="img/products_4.png">
                      </div>
                    </div>
                    <div class="col-lg-10 col-12">
                      <div class="share-socialmedia">
                        <ul class="list-inline">
                          <li class="list-inline-item"><label>Share :</label></li>
                          <li class="list-inline-item"><a href="" class="share-link facebook"></a></li>
                          <li class="list-inline-item"><a href="" class="share-link instagram"></a></li>
                          <li class="list-inline-item"><a href="" class="share-link twitter"></a></li>
                          <li class="list-inline-item"><a href="" class="share-link googleplus"></a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-lg-10 col-12">
                      <div class="content">
                        <div class="row">
                          <div class="col-12">
                            <div class="title">
                              Sangobion Fizz
                            </div>
                          </div>
                          <div class="col-lg-4 col-12">
                            <div class="category">
                              <ul class="list-unstyled">
                                <li>Work Productivity</li>
                                <li>Cognition</li>
                              </ul>
                            </div>
                          </div>
                          <div class="col-lg-8 col-12">
                            <div class="desc">
                              <ul>
                              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                              <li>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</li>
                              <li>Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </li>
                              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="product-detail" style="display: none;" id="work-productivity-2">
                <div class="container">
                  <div class="row">
                    <div class="col-xl-7 col-lg-8 col-md-10 col-12">
                      <div class="image">
                        <img src="img/products_6.png">
                      </div>
                    </div>
                    <div class="col-lg-10 col-12">
                      <div class="share-socialmedia">
                        <ul class="list-inline">
                          <li class="list-inline-item"><label>Share :</label></li>
                          <li class="list-inline-item"><a href="" class="share-link facebook"></a></li>
                          <li class="list-inline-item"><a href="" class="share-link instagram"></a></li>
                          <li class="list-inline-item"><a href="" class="share-link twitter"></a></li>
                          <li class="list-inline-item"><a href="" class="share-link googleplus"></a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-lg-10 col-12">
                      <div class="content">
                        <div class="row">
                          <div class="col-12">
                            <div class="title">
                              Sangobion Fizz
                            </div>
                          </div>
                          <div class="col-lg-4 col-12">
                            <div class="category">
                              <ul class="list-unstyled">
                                <li>Work Productivity</li>
                                <li>Cognition</li>
                              </ul>
                            </div>
                          </div>
                          <div class="col-lg-8 col-12">
                            <div class="desc">
                              <ul>
                              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                              <li>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</li>
                              <li>Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </li>
                              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="product-detail" style="display: none;" id="growth-2">
                <div class="container">
                  <div class="row">
                    <div class="col-xl-7 col-lg-8 col-md-10 col-12">
                      <div class="image">
                        <img src="img/products_5.png">
                      </div>
                    </div>
                    <div class="col-lg-10 col-12">
                      <div class="share-socialmedia">
                        <ul class="list-inline">
                          <li class="list-inline-item"><label>Share :</label></li>
                          <li class="list-inline-item"><a href="" class="share-link facebook"></a></li>
                          <li class="list-inline-item"><a href="" class="share-link instagram"></a></li>
                          <li class="list-inline-item"><a href="" class="share-link twitter"></a></li>
                          <li class="list-inline-item"><a href="" class="share-link googleplus"></a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-lg-10 col-12">
                      <div class="content">
                        <div class="row">
                          <div class="col-12">
                            <div class="title">
                              Sangobion Fizz
                            </div>
                          </div>
                          <div class="col-lg-4 col-12">
                            <div class="category">
                              <ul class="list-unstyled">
                                <li>Work Productivity</li>
                                <li>Cognition</li>
                              </ul>
                            </div>
                          </div>
                          <div class="col-lg-8 col-12">
                            <div class="desc">
                              <ul>
                              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                              <li>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</li>
                              <li>Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </li>
                              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="product-detail" style="display: none;" id="immunity-2">
                <div class="container">
                  <div class="row">
                    <div class="col-xl-7 col-lg-8 col-md-10 col-12">
                      <div class="image">
                        <img src="img/products_7.png">
                      </div>
                    </div>
                    <div class="col-lg-10 col-12">
                      <div class="share-socialmedia">
                        <ul class="list-inline">
                          <li class="list-inline-item"><label>Share :</label></li>
                          <li class="list-inline-item"><a href="" class="share-link facebook"></a></li>
                          <li class="list-inline-item"><a href="" class="share-link instagram"></a></li>
                          <li class="list-inline-item"><a href="" class="share-link twitter"></a></li>
                          <li class="list-inline-item"><a href="" class="share-link googleplus"></a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-lg-10 col-12">
                      <div class="content">
                        <div class="row">
                          <div class="col-12">
                            <div class="title">
                              Sangobion Fizz
                            </div>
                          </div>
                          <div class="col-lg-4 col-12">
                            <div class="category">
                              <ul class="list-unstyled">
                                <li>Work Productivity</li>
                                <li>Cognition</li>
                              </ul>
                            </div>
                          </div>
                          <div class="col-lg-8 col-12">
                            <div class="desc">
                              <ul>
                              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                              <li>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</li>
                              <li>Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </li>
                              <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12">
            <div class="product-benefits">
              <div class="row">
                <div class="col-lg-3 col-md-6 col-12 product-benefit-col">
                  <div class="product-benefit-item">
                    <div class="image wow fadeInLeft">
                      <img src="img/product_benefit_1.png">
                    </div>
                    <div class="desc wow fadeInRight" data-wow-delay="0.1s">
                      <p>Supports Optimum Blood Health</p>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12 product-benefit-col">
                  <div class="product-benefit-item">
                    <div class="image wow fadeInLeft">
                      <img src="img/product_benefit_2.png">
                    </div>
                    <div class="desc wow fadeInRight" data-wow-delay="0.1s">
                      <p>Halal Certified</p>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12 product-benefit-col">
                  <div class="product-benefit-item">
                    <div class="image wow fadeInLeft">
                      <img src="img/DR_icon.png">
                    </div>
                    <div class="desc wow fadeInRight" data-wow-delay="0.1s">
                      <p>Most recommended for Iron Deficiency Anemia</p>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12 product-benefit-col">
                  <div class="product-benefit-item">
                    <div class="image wow fadeInLeft">
                      <img src="img/50yrs_icon.png">
                    </div>
                    <div class="desc wow fadeInRight" data-wow-delay="0.1s">
                      <p>Trusted by Doctors for over 50 years</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php include('footer.php');?>
  </main>
  <?php include('script.php');?>
  
</body>
</html>

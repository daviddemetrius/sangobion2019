<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <?php include('metatag.php');?>
  <title>Home | SANGOBION</title>
  <link rel="icon" href="img/favicon.ico">
  <?php include('stylesheet.php');?>
</head>

<body>
  <main class="main-wrap" id="contactus">
    <?php $page = 'contactus';include('header.php');?>
    <!-- body start -->
    <section class="section">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="contactus-wrapper wow fadeInDown">
              <form>
                <div class="subtitle wow fadeInDown" data-wow-delay="0.25s">
                  Reason for writing
                </div>
                <div class="form-group with-custom-blood wow fadeInUp" data-wow-delay="0.5s">
                  <!-- <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input" id="customRadio1" name="reason-radio">
                    <label class="custom-control-label" for="customRadio1">Where to buy</label>
                  </div> 
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input" id="customRadio2" name="reason-radio">
                    <label class="custom-control-label" for="customRadio2">About the products</label>
                  </div> 
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input" id="customRadio3" name="reason-radio">
                    <label class="custom-control-label" for="customRadio3">Complaints or Suggestions</label>
                  </div> 
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input" id="customRadio4" name="reason-radio">
                    <label class="custom-control-label" for="customRadio4">General issue</label>
                  </div>  -->
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input" id="customRadio1" name="reason-radio">
                    <label class="custom-control-label" for="customRadio1">About the products</label>
                  </div> 
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input" id="customRadio2" name="reason-radio">
                    <label class="custom-control-label" for="customRadio2">Complaints or suggestions</label>
                  </div> 
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input" id="customRadio3" name="reason-radio">
                    <label class="custom-control-label" for="customRadio3">General issue</label>
                  </div> 
                </div>
                <div class="subtitle wow fadeInDown" data-wow-delay="0.75s">
                  How can we help?
                </div>
                <div class="row">
                  <div class="col-lg-5 col-12">
                    <div class="form-group wow fadeInUp" data-wow-delay="1s">
                      <input type="text" class="form-control" placeholder="Fullname">
                    </div>
                    <div class="form-group wow fadeInUp" data-wow-delay="1.1s">
                      <input type="text" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group wow fadeInUp" data-wow-delay="1.2s">
                      <input type="text" class="form-control" placeholder="Phone Number">
                    </div>
                    <div class="form-group gender wow fadeInUp" data-wow-delay="1.3s">
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="genderRadio1" name="gender-radio">
                        <label class="custom-control-label" for="genderRadio1">Male</label>
                      </div> 
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="genderRadio2" name="gender-radio">
                        <label class="custom-control-label" for="genderRadio2">Female</label>
                      </div> 
                    </div>
                  </div>
                  <div class="col-lg-7 col-12">
                    <div class="form-group wow fadeInUp" data-wow-delay="1.4s">
                      <textarea class="form-control" placeholder="Your Message Here" rows="9"></textarea>
                    </div>
                    <div class="row justify-content-between align-items-center">
                      <div class="col">
                        <img src="img/captcha_gif.gif">
                      </div>
                      <div class="col">
                        <div class="form-submit form-group wow fadeInUp mb-0" data-wow-delay="1.5s">
                          <button type="submit" class="btn btn-red">Submit</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php include('footer.php');?>
  </main>
  <?php include('script.php');?>
</body>
</html>

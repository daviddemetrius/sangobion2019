<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <?php include('metatag.php');?>
  <title>Home | SANGOBION</title>
  <link rel="icon" href="img/favicon.ico">
  <?php include('stylesheet.php');?>
</head>

<body>
  <main class="main-wrap" id="article-details">
    <?php $page = 'article';include('header.php');?>
    <!-- body start -->
    <section class="section main-detail">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="detail-wrapper">
              <div class="row">
                <div class="col-lg-7 col-12 order-lg-2">
                  <div class="share-socialmedia d-none wow fadeInDown" data-wow-delay="0.25s">
                    <ul class="list-inline">
                      <li class="list-inline-item"><label>Share This :</label></li>
                      <li class="list-inline-item"><a href="" class="share-link facebook"></a></li>
                      <li class="list-inline-item"><a href="" class="share-link instagram"></a></li>
                      <li class="list-inline-item"><a href="" class="share-link twitter"></a></li>
                      <li class="list-inline-item"><a href="" class="share-link googleplus"></a></li>
                    </ul>
                  </div>
                  <div class="detail-main wow fadeInUp"  data-wow-delay="0.5s">
                    <div class="title">
                    Not sure if you matched the symptoms? Talk to our doctors!
                    </div>
                    <div class="image">
                      <img src="img/articledetail_1.png">
                    </div>
                  </div>
                </div>
                <div class="col-12 order-lg-3">
                  <div class="content wow fadeInUp" data-wow-delay="0.5s">
                    <div class="desc">
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
                      <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?</p>
                    </div>
                  </div>
                </div>
                <div class="col-12 order-lg-4">
                  <div class="content wow fadeInUp" data-wow-delay="0.5s">
                    <div class="row">
                      <div class="col-12">
                        <div class="subtitle">
                          Blood Chemistry
                        </div>
                      </div>
                      <div class="col-lg-6 col-12">
                        <div class="desc">
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, </p>
                        </div>
                      </div>
                      <div class="col-lg-6 col-12">
                        <div class="image">
                          <img src="img/articledetail_2.png">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-5 col-12 order-lg-1">
                  <div class="detail-point wow fadeInDown">
                    <div class="detail-point-heading">
                      Learn about other health need:
                    </div>
                    <ul class="list-unstyled">
                      <li class="active"><a href="">Anemia</a></li>
                      <li><a href="">Iron Deficiency</a></li>
                      <li><a href="">Menstrual Pain</a></li>
                      <li><a href="">Performance</a></li>
                    </ul>
                    <div class="detail-point-heading">
                      Also learn more about:
                    </div>
                    <ul class="list-unstyled">
                      <li><a href="">Signs & Symptoms</a></li>
                      <li><a href="">Health consequences of Anemia</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="section with-background">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-12">
            <div class="related-article">
              <div class="row">
                <div class="col-12">
                  <div class="section-heading wow fadeInDown">
                    <div class="subheading">
                      Related Articles
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 col-12 related-col">
                  <div class="related-item wow fadeInUp" data-wow-delay="0.25s">
                    <div class="image">
                      <img src="img/related_1.png">
                    </div>
                    <div class="separator"></div>
                    <div class="text">
                      <div class="title">
                        How is iron deficiency anemia
                      </div>
                      <div class="desc">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                      </div>
                    </div>
                    <div class="link">
                      <a href="" class="readmore">Read More</a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 col-12 related-col">
                  <div class="related-item wow fadeInUp" data-wow-delay="0.25s">
                    <div class="image">
                      <img src="img/related_2.png">
                    </div>
                    <div class="separator"></div>
                    <div class="text">
                      <div class="title">
                        How is IDA treated and prevented
                      </div>
                      <div class="desc">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                      </div>
                    </div>
                    <div class="link">
                      <a href="" class="readmore">Read More</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="section slider-overflow">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section-heading wow fadeInDown">
              <div class="heading">
                Recommended Products
              </div>
            </div>
            <div class="recommended-slider wow fadeInUp" data-wow-delay="0.25s">
              <div class="recommended-col">
                <div class="recommended-item">
                  <div class="text">
                    <div class="title">Product for <strong>Adults</strong></div>
                  </div>
                  <div class="image">
                    <img src="img/img-5.png" alt="sangobion fit">
                  </div>
                  <div class="link">
                    <a href="#">						
                      <div class="btn-learn-more">View Product</div>
                      <div class="icon-arrow-down"></div>						
                    </a>
                  </div>
                </div>
              </div>
              <div class="recommended-col">
                <div class="recommended-item">
                  <div class="text">
                    <div class="title">Product for <strong>Adults</strong></div>
                  </div>
                  <div class="image">
                    <img src="img/img-5.png" alt="sangobion fit">
                  </div>
                  <div class="link">
                    <a href="#">						
                      <div class="btn-learn-more">View Product</div>
                      <div class="icon-arrow-down"></div>						
                    </a>
                  </div>
                </div>
              </div>
              <div class="recommended-col">
                <div class="recommended-item">
                  <div class="text">
                    <div class="title">Product for <strong>Adults</strong></div>
                  </div>
                  <div class="image">
                    <img src="img/img-5.png" alt="sangobion fit">
                  </div>
                  <div class="link">
                    <a href="#">						
                      <div class="btn-learn-more">View Product</div>
                      <div class="icon-arrow-down"></div>						
                    </a>
                  </div>
                </div>
              </div>
              <div class="recommended-col">
                <div class="recommended-item">
                  <div class="text">
                    <div class="title">Product for <strong>Adults</strong></div>
                  </div>
                  <div class="image">
                    <img src="img/img-5.png" alt="sangobion fit">
                  </div>
                  <div class="link">
                    <a href="#">						
                      <div class="btn-learn-more">View Product</div>
                      <div class="icon-arrow-down"></div>						
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php include('footer.php');?>
  </main>
  <?php include('script.php');?>
</body>
</html>

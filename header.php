<!-- header start -->
<header class="fixed-top">
  <div class="top-nav">
    <ul class="list-inline logo-group">
      <li class="list-inline-item"><a href="product.php#work-productivity-1" class="logo-item sangobion"></a></li>
      <li class="list-inline-item"><a href="product.php#work-productivity-1" class="logo-item vitatonik"></a></li>
      <li class="list-inline-item"><a href="product.php#growth-2" class="logo-item baby"></a></li>
      <li class="list-inline-item"><a href="product.php#growth-2" class="logo-item kids"></a></li>
      <li class="list-inline-item"><a href="product.php#growth-2" class="logo-item fizz"></a></li>
      <li class="list-inline-item d-none"><a href="product.php#immunity-1" class="logo-item femine-menstrupain"></a></li>
      <li class="list-inline-item d-none"><a href="product.php#cognition" class="logo-item sangobion-fit"></a></li>
    </ul>
    <div class="globalsearch-wrapper">
      <form action="" autocomplete="on">
        <input class="form-control hidden" id="search" name="search" type="text" placeholder="What're we looking for ?">
        <input class="btn btn-search" id="search_submit" value="Rechercher" type="">
      </form>
    </div>
  </div>
  <nav class="main-nav navbar navbar-expand-xl navbar-light">
    <a class="navbar-brand" href="index.php"><img src="img/logo-sangobion.png"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item <?php if($page=='blood-health-expert'){echo 'active';}?>">
          <a class="nav-link" href="blood-health-expert.php">Blood Health Expert</a>
        </li>
        <li class="nav-item <?php if($page=='product'){echo 'active';}?>">
          <a class="nav-link" href="product.php">Products</a>
        </li>
        <li class="nav-item <?php if($page=='signandsymptoms'){echo 'active';}?>">
          <a class="nav-link" href="signandsymptoms.php">Signs & Symptoms</a>
        </li>
        <li class="nav-item dropdown <?php if($page=='article'){echo 'active';}?>">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Learn About
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="article-a.php">Anemia</a>
            <a class="dropdown-item" href="article-b.php">Iron deficiency</a>
            <a class="dropdown-item" href="article-d.php">Menstrual Pain</a>
            <a class="dropdown-item" href="article-c.php">Performance</a>
          </div>
        </li>
        <li class="nav-item selection d-none">
          <span class="nav-link">Learn About : </span>
          <div class="select-arrow">
            <select class="custom-select">
              <option disabled selected>Select a condition</option>
              <option value="article-a.php">Anemia</option>
              <option value="article-b.php">Iron deficiency</option>
              <option value="article-d.php">Menstrual Pain</option>
              <option value="article-c.php">Performance</option>
            </select>
          </div>
        </li>
        <li class="nav-item <?php if($page=='campaign'){echo 'active';}?>">
          <a class="nav-link" href="campaign.php">Campaign</a>
        </li>
        <li class="nav-item <?php if($page=='contactus'){echo 'active';}?>">
          <a class="nav-link" href="contactus.php">Contact Us</a>
        </li>
      </ul>
    </div>
  </nav>
</header>
<!-- header end -->

<div class="action-wrapper">
  <ul class="list-unstyled">
    <li class="action-item">
      <a href="where-to-buy.php"><img src="img/buy.png"></a>
    </li>
    <li class="action-item">
      <a class="addthis_button_compact" href=""><img src="img/share.png"></a>
    </li>
  </ul>
</div>
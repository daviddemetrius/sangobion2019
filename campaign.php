<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <?php include('metatag.php');?>
  <title>Home | SANGOBION</title>
  <link rel="icon" href="img/favicon.ico">
  <?php include('stylesheet.php');?>
</head>

<body>
  <main class="main-wrap" id="campaign">
    <?php $page = 'campaign';include('header.php');?>
    <!-- body start -->
    <section class="section">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="category-tabs wow fadeInDown d-none">
              <div class="campaign-tab" data-target="all">
                All
              </div>
              <div class="campaign-tab" data-target="campaign">
                Campaign
              </div>
              <div class="campaign-tab" data-target="anemia-advocacy">
                Anemia Advocacy
              </div>
            </div>
            <div class="campaign-slider wow fadeInUp" data-wow-delay="0.25s">
              <div class="slide all campaign">
                <div class="image">
                  <img src="img/campaign_1.png">
                  <div class="overlay">
                    <div class="slide-detail">
                      <div class="title">
                      Sangobion Femine Mentrupain Ajak Perempuan Indonesia Tetap Aktif selama Menstruasi.
                      </div>
                      <div class="desc">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="campaign-nav">
                  <div class="item-nav all campaign">
                    <img src="img/campaign_1.png">
                  </div>
                  <div class="item-nav all campaign">
                    <img src="img/campaign_2.png">
                  </div>
                  <div class="item-nav all campaign">
                    <img src="img/campaign_3.png">
                  </div>
                  <div class="item-nav all campaign">
                    <img src="img/campaign_4.png">
                  </div>
                  <div class="item-nav all campaign">
                    <img src="img/campaign_5.png">
                  </div>
                  <div class="item-nav all campaign">
                    <img src="img/campaign_6.png">
                  </div>
                  <div class="item-nav all campaign">
                    <img src="img/campaign_7.png">
                  </div>
                </div>
              </div>
              <div class="slide all campaign">
                <div class="image">
                  <img src="img/campaign_2.png">
                  <div class="overlay">
                    <div class="slide-detail">
                      <div class="title">
                      Sangobion Femine Mentrupain Ajak Perempuan Indonesia Tetap Aktif selama Menstruasi.
                      </div>
                      <div class="desc">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="campaign-nav wow fadeInUp" data-wow-delay="0.5s">
                  <div class="item-nav all campaign">
                    <img src="img/campaign_2.png">
                  </div>
                  <div class="item-nav all campaign">
                    <img src="img/campaign_3.png">
                  </div>
                  <div class="item-nav all campaign">
                    <img src="img/campaign_4.png">
                  </div>
                  <div class="item-nav all campaign">
                    <img src="img/campaign_5.png">
                  </div>
                  <div class="item-nav all campaign">
                    <img src="img/campaign_6.png">
                  </div>
                  <div class="item-nav all campaign">
                    <img src="img/campaign_7.png">
                  </div>
                  <div class="item-nav all campaign">
                    <img src="img/campaign_8.png">
                  </div>
                </div>
              </div>
              <div class="slide all anemia-advocacy">
                <div class="image">
                  <img src="img/campaign_3.png">
                  <div class="overlay">
                    <div class="slide-detail">
                      <div class="title">
                      Sangobion Femine Mentrupain Ajak Perempuan Indonesia Tetap Aktif selama Menstruasi.
                      </div>
                      <div class="desc">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="campaign-nav wow fadeInUp" data-wow-delay="0.5s">
                  <div class="item-nav all anemia-advocacy">
                    <img src="img/campaign_3.png">
                  </div>
                  <div class="item-nav all anemia-advocacy">
                    <img src="img/campaign_4.png">
                  </div>
                  <div class="item-nav all anemia-advocacy">
                    <img src="img/campaign_5.png">
                  </div>
                  <div class="item-nav all anemia-advocacy">
                    <img src="img/campaign_6.png">
                  </div>
                  <div class="item-nav all anemia-advocacy">
                    <img src="img/campaign_7.png">
                  </div>
                  <div class="item-nav all anemia-advocacy">
                    <img src="img/campaign_8.png">
                  </div>
                  <div class="item-nav all anemia-advocacy">
                    <img src="img/campaign_9.png">
                  </div>
                </div>
              </div>
            </div>
            <div class="campaign-nav wow fadeInUp d-none" data-wow-delay="0.5s">
              <div class="item-nav all campaign">
                <img src="img/campaign_nav_1.png">
              </div>
              <div class="item-nav all campaign">
                <img src="img/campaign_nav_2.png">
              </div>
              <div class="item-nav all anemia-advocacy">
                <img src="img/campaign_nav_3.png">
              </div>
              <div class="item-nav all campaign">
                <img src="img/campaign_nav_4.png">
              </div>
              <div class="item-nav all anemia-advocacy">
                <img src="img/campaign_nav_5.png">
              </div>
              <div class="item-nav all anemia-advocacy">
                <img src="img/campaign_nav_6.png">
              </div>
              <div class="item-nav all campaign">
                <img src="img/campaign_nav_7.png">
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php include('footer.php');?>
  </main>
  <?php include('script.php');?>
</body>
</html>

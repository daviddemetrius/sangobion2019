<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <?php include('metatag.php');?>
  <title>Home | SANGOBION</title>
  <link rel="icon" href="img/favicon.ico">
  <?php include('stylesheet.php');?>
</head>

<body>
  <main class="main-wrap" id="where-to-buy">
    <?php include('header.php');?>
    <!-- body start -->
    <section class="section">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section-heading text-center">
              <div class="subheading wow fadeInDown">
                Where to buy our products
              </div>
              <Div class="separator wow fadeInDown" data-wow-delay="0.25s"></Div>
            </div>
            <div class="wheretobuy-wrapper wow fadeInUp" data-wow-delay="0.5s">
              <ul class="list-inline">
                <li class="list-inline-item"><a href="" class="tokopedia"></a></li>
                <li class="list-inline-item"><a href="" class="lazada"></a></li>
                <li class="list-inline-item"><a href="" class="kalbestore"></a></li>
                <li class="list-inline-item"><a href="" class="amazon"></a></li>
                <li class="list-inline-item"><a href="" class="shopee"></a></li>
              </ul>
            </div>
          </div>
          <div class="col-12">
            <div class="section-heading text-center wow fadeInDown" data-wow-delay="0.75s">
              <div class="subheading">
                Find your nearest store
              </div>
            </div>
            <div class="maps wow fadeInUp" data-wow-delay="1s">
            <iframe scrolling="no" src="https://www.google.com/maps/d/u/0/embed?mid=1Akqzc8OOUPH_xthfobMc4KPUIAaulBbj" width="100%" height="480"></iframe>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php include('footer.php');?>
  </main>
  <?php include('script.php');?>
</body>
</html>

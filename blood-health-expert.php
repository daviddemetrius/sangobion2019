<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <?php include('metatag.php');?>
  <title>Home | SANGOBION</title>
  <link rel="icon" href="img/favicon.ico">
  <?php include('stylesheet.php');?>
</head>

<body>
  <main class="main-wrap" id="article">
    <?php $page = 'blood-health-expert';include('header.php');?>
    <!-- body start -->
    <section class="home-banner wowParent animateOnce">
      <div id="layerslider" style="width:1366px;height:500px;margin:0 auto;" class="wow fadeIn d-none d-sm-block">
        <div class="ls-slide" data-ls="duration:8000;transition2d:5;">
          <img class="ls-bg" src="img/a-bg-1.png" alt="background">
          <img class="ls-l" style="top:0;left:0;white-space:nowrap;" src="img/bloodslider-1.png" alt="">	
          <img class="ls-l" style="top:;left:505px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:0;durationin:1800;delayin:200;scaleyin:0%;transformoriginin:0% top 0;" src="img/bloodslider-2.png" alt="">
          <img class="ls-l" style="top:70px;left:100px;white-space:nowrap;" src="img/bloodslider-0.png" alt="">
          <div class="ls-l hslider-title" style="top:80px;left:820px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1400;delayin:600;">
            <p>It's in your blood</p>
          </div>
          <div class="ls-l hslider-image" style="top:220px;left:800px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1400;delayin:600;">
            <img src="img/bloodslider-3.png">
          </div>
          <div class="ls-l hslider-desc" style="top:220px;left:920px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1400;delayin:600;">
            <p>Lorem ipsum dolor sit amet,<br>consectetur adipiscing elit,<br>sed do eiusmod tempor incididunt ut labore<br>et dolore magna aliqua.</p>
          </div>
          <div class="ls-l" style="top:330px;left:920px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:bottom;durationin:1400;delayin:600;">
            <a href="ask-the-expert.php" class="btn btn-white">Ask the expert</a>
          </div>
        </div>
  <!--
        <div class="ls-slide" data-ls="duration:8000;transition2d:5;">
          <img class="ls-bg" src="img/bg-1.jpg" alt="background">
          <img class="ls-l" style="top:0;left:0;white-space:nowrap;" data-ls="offsetxin:left;offsetyin:0;durationin:1600;delayin:200;" src="img/hslider-4.png" alt="">	
          <img class="ls-l" style="top:0;left:500px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:0;durationin:1800;delayin:600;scaleyin:0%;transformoriginin:0% top 0;" src="img/hslider-3-1.png" alt="">
          <img class="ls-l" style="top:0;left:500px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:0;durationin:1800;delayin:600;scaleyin:0%;transformoriginin:0% bottom 0;" src="img/hslider-3-2.png" alt="">
          <img class="ls-l" style="top:170px;left:210px;white-space:nowrap;" data-ls="offsetxin:left;offsetyin:0;durationin:2000;delayin:1400;" src="img/hslider-5.png" alt="">
          <div class="ls-l hslider-title" style="top:100px;left:820px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1400;delayin:1800;">
            <p>ARE</p>
            <p>YOU AT RISK ?</p>
          </div>
          <div class="ls-l hslider-desc" style="top:260px;left:820px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1400;delayin:2000;">
            <p>Do you know ?</p>
            <p>Indonesia is a country with a high level of anemia</p>
            <p>cases. Iron deficiency is one of the biggest</p>
            <p>nutritional problems in Indonesia.</p>
            <div class="space-2"></div>
            <p>Are you at risk for anemia?</p>
          </div>
          <div class="ls-l" style="top:440px;left:820px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:bottom;durationin:1400;delayin:2200;">
            <a href="#" class="btn-grad-2">Check up now !</a>
          </div>
        </div>
        <div class="ls-slide" data-ls="duration:8000;transition2d:5;">
          <img class="ls-bg" src="img/bg-1.jpg" alt="background">
          <img class="ls-l" style="top:0;left:0;white-space:nowrap;" data-ls="offsetxin:left;offsetyin:0;durationin:1600;delayin:200;" src="img/hslider-6.png" alt="">	
          <img class="ls-l" style="top:0;left:500px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:0;durationin:1800;delayin:600;scaleyin:0%;transformoriginin:0% top 0;" src="img/hslider-3-1.png" alt="">
          <img class="ls-l" style="top:0;left:500px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:0;durationin:1800;delayin:600;scaleyin:0%;transformoriginin:0% bottom 0;" src="img/hslider-3-2.png" alt="">
          <img class="ls-l" style="top:435px;left:0px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:bottom;durationin:1400;delayin:1400;" src="img/hslider-7.png" alt="">
          <img class="ls-l" style="top:150px;left:450px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:-100;durationin:1400;delayin:1800;" src="img/hslider-8.png" alt="">
          <img class="ls-l" style="top:120px;left:255px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:-100;durationin:1400;delayin:2000;" src="img/hslider-9.png" alt="">
          <img class="ls-l" style="top:380px;left:25px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:0;durationin:1400;delayin:2200;scalein:.8;" src="img/hslider-10.png" alt="">
          <img class="ls-l" style="top:460px;left:360px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:0;durationin:1400;delayin:2300;" src="img/hslider-11.png" alt="">
          <div class="ls-l hslider-title" style="top:100px;left:820px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1400;delayin:1800;">
            <p>SANGOBION</p>
            <p>VITA-TONIK?</p>
          </div>
          <div class="ls-l hslider-desc" style="top:260px;left:820px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1400;delayin:2000;">
            <p>Jaga terus kesehatanmu, jangan sampai terkena</p>
            <p>5L. Ayo minum Sangobion selalu! Kini hadir dalam </p>
            <p>bentuk sirup.</p>
            <div class="space-2"></div>
            <p>Generasi Produktif, Generasi Bebas Anemia</p>
          </div>
          <div class="ls-l" style="top:410px;left:820px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:bottom;durationin:1400;delayin:2200;">
            <a href="#" class="btn-grad-2">Buy Now !</a>
          </div>
        </div>
  -->
      </div>
      <div id="layerslider_mobile" style="width:1366px;height:550px;margin:0 auto;" class="wow fadeIn d-block d-sm-none">
        <div class="ls-slide" data-ls="duration:8000;transition2d:5;">
        <img class="ls-bg" src="img/a-bg-1.png" alt="background">
          <img class="ls-l" style="top:0;left:-300px;white-space:nowrap;" src="img/bloodslider-1.png" alt="">	
          <img class="ls-l" style="top:;left:200px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:0;durationin:1800;delayin:200;scaleyin:0%;transformoriginin:0% top 0;" src="img/bloodslider-2.png" alt="">
          <img class="ls-l" style="top:60px;left:20%;white-space:nowrap;" src="img/bloodslider-0.png" alt="">
          <div class="ls-l hslider-separator d-none" style="top:575px;left:0;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1400;delayin:600;"></div>
          <div class="ls-l hslider-title" style="top:10%;left:50%;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1400;delayin:600;">
            <p>It's in your blood</p>
          </div>
          <div class="ls-l hslider-image" style="top:32%;left:45%;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1400;delayin:600;">
            <img src="img/bloodslider-3.png">
          </div>
          <div class="ls-l hslider-desc" style="top:32%;left:80%;white-space:initial;width: 40%;" data-ls="offsetxin:0;offsetyin:top;durationin:1400;delayin:600;">
            <p>Lorem ipsum dolor sit amet,<br>consectetur adipiscing elit,<br>sed do eiusmod tempor incididunt ut labore<br>et dolore magna aliqua.</p>
          </div>
          <div class="ls-l" style="top:75%;left:65%;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:bottom;durationin:1400;delayin:600;">
            <a href="ask-the-expert.php" class="btn btn-white">Ask the expert</a>
          </div>
        </div>
      </div>
    </section>
    <section class="section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-xl-10 col-lg-11 col-12">
            <div class="intro">
              <div class="row">
                <div class="col-lg-6 col-12">
                  <div class="title mt-0 pr-5 mr-5 wow fadeInLeft">
                    Facts about iron absorption
                  </div>
                </div>
                <div class="col-lg-6 col-12">
                  <div class="desc wow fadeInRight" data-wow-delay="0.25s">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                  </div>
                  <div class="link wow fadeInRight" data-wow-delay="0.5s">
                    <a href="" class="btn btn-red">Read More</a>
                  </div>
                </div>
              </div>              
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="with-background">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="overlap-wrapper">
              <div class="image wow fadeInUp">
                <img src="img/blood-overlap-1.png">
              </div>
              <div class="intro wow fadeInDown" data-wow-delay="0.25s">
                <div class="title wow fadeInDown" data-wow-delay="0.5s">
                Iron deficiency and the developing brain
                </div>
                <div class="desc wow fadeInDown" data-wow-delay="0.75s">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                </div>
                <div class="link wow fadeInUp" data-wow-delay="1s">
                  <a href="" class="btn btn-white">Read More</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="section">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="thumbnail-wrapper">
              <div class="row">
                <div class="col-12 thumbnail-col">
                  <div class="thumbnail-item">
                    <div class="row">
                      <div class="col-lg-6 col-12">
                        <div class="image wow fadeInLeft">
                          <img src="img/thumbnail_3.png">
                        </div>
                      </div>
                      <div class="col-lg-6 col-12">
                        <div class="text wow fadeInRight" data-wow-delay="0.25s">
                          <div class="title">
                            How is iron deficiency anemia diagnosed?
                          </div>
                          <div class="desc">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                          </div>
                          <div class="link">
                            <a href="" class="btn btn-red">Read More</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="section slider-overflow">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section-heading wow fadeInDown">
              <div class="heading">
                Recommended Products
              </div>
            </div>
            <div class="recommended-slider wow fadeInUp" data-wow-delay="0.25s">
              <div class="recommended-col">
                <div class="recommended-item">
                  <div class="text">
                    <div class="title">Product for <strong>Adults</strong></div>
                  </div>
                  <div class="image">
                    <img src="img/img-5.png" alt="sangobion fit">
                  </div>
                  <div class="link">
                    <a href="#">						
                      <div class="btn-learn-more">View Product</div>
                      <div class="icon-arrow-down"></div>						
                    </a>
                  </div>
                </div>
              </div>
              <div class="recommended-col">
                <div class="recommended-item">
                  <div class="text">
                    <div class="title">Product for <strong>Adults</strong></div>
                  </div>
                  <div class="image">
                    <img src="img/img-5.png" alt="sangobion fit">
                  </div>
                  <div class="link">
                    <a href="#">						
                      <div class="btn-learn-more">View Product</div>
                      <div class="icon-arrow-down"></div>						
                    </a>
                  </div>
                </div>
              </div>
              <div class="recommended-col">
                <div class="recommended-item">
                  <div class="text">
                    <div class="title">Product for <strong>Adults</strong></div>
                  </div>
                  <div class="image">
                    <img src="img/img-5.png" alt="sangobion fit">
                  </div>
                  <div class="link">
                    <a href="#">						
                      <div class="btn-learn-more">View Product</div>
                      <div class="icon-arrow-down"></div>						
                    </a>
                  </div>
                </div>
              </div>
              <div class="recommended-col">
                <div class="recommended-item">
                  <div class="text">
                    <div class="title">Product for <strong>Adults</strong></div>
                  </div>
                  <div class="image">
                    <img src="img/img-5.png" alt="sangobion fit">
                  </div>
                  <div class="link">
                    <a href="#">						
                      <div class="btn-learn-more">View Product</div>
                      <div class="icon-arrow-down"></div>						
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php include('footer.php');?>
  </main>
  <?php include('script.php');?>
</body>
</html>

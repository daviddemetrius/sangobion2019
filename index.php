<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <?php include('metatag.php');?>
  <title>Home | SANGOBION</title>
  <link rel="icon" href="img/favicon.ico">
  <?php include('stylesheet.php');?>
</head>

<body>
  <main class="main-wrap">
    <?php include('header.php');?>
    <!-- body start -->
    <section class="home-banner wowParent animateOnce">
      <div id="layerslider" style="width:1366px;height:652px;margin:0 auto;" class="wow fadeIn d-none d-sm-block">
        <div class="ls-slide" data-ls="duration:8000;transition2d:5;">
          <img class="ls-bg" src="img/bg-1.jpg" alt="background">
          <img class="ls-l" style="top:0;left:0;white-space:nowrap;" src="img/hslider-2.png" alt="">	
          <img class="ls-l" style="top:0;left:500px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:0;durationin:1800;delayin:200;scaleyin:0%;transformoriginin:0% top 0;" src="img/hslider-3-1.png" alt="">
          <img class="ls-l" style="top:0;left:500px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:0;durationin:1800;delayin:200;scaleyin:0%;transformoriginin:0% bottom 0;" src="img/hslider-3-2.png" alt="">
          <img class="ls-l" style="top:50px;left:20px;white-space:nowrap;width:800px;" src="img/hslider-1.png" alt="">
          <div class="ls-l hslider-title" style="top:100px;left:820px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1400;delayin:600;">
            <p>The Blood</p>
            <p>Expert</p>
          </div>
          <div class="ls-l hslider-desc" style="top:260px;left:820px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1400;delayin:600;">
            <p>Dulu aktivitas saya sering terganggu karena kurang darah (anemia)<br>Sekarang saya selalu segar bersemangat</p>
            <p><strong>Mau tau Tips saya?</strong></p>
          </div>
          <div class="ls-l" style="top:420px;left:820px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:bottom;durationin:1400;delayin:600;">
            <a href="#" class="btn btn-white">Read More</a>
          </div>
        </div>
  <!--
        <div class="ls-slide" data-ls="duration:8000;transition2d:5;">
          <img class="ls-bg" src="img/bg-1.jpg" alt="background">
          <img class="ls-l" style="top:0;left:0;white-space:nowrap;" data-ls="offsetxin:left;offsetyin:0;durationin:1600;delayin:200;" src="img/hslider-4.png" alt="">	
          <img class="ls-l" style="top:0;left:500px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:0;durationin:1800;delayin:600;scaleyin:0%;transformoriginin:0% top 0;" src="img/hslider-3-1.png" alt="">
          <img class="ls-l" style="top:0;left:500px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:0;durationin:1800;delayin:600;scaleyin:0%;transformoriginin:0% bottom 0;" src="img/hslider-3-2.png" alt="">
          <img class="ls-l" style="top:170px;left:210px;white-space:nowrap;" data-ls="offsetxin:left;offsetyin:0;durationin:2000;delayin:1400;" src="img/hslider-5.png" alt="">
          <div class="ls-l hslider-title" style="top:100px;left:820px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1400;delayin:1800;">
            <p>ARE</p>
            <p>YOU AT RISK ?</p>
          </div>
          <div class="ls-l hslider-desc" style="top:260px;left:820px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1400;delayin:2000;">
            <p>Do you know ?</p>
            <p>Indonesia is a country with a high level of anemia</p>
            <p>cases. Iron deficiency is one of the biggest</p>
            <p>nutritional problems in Indonesia.</p>
            <div class="space-2"></div>
            <p>Are you at risk for anemia?</p>
          </div>
          <div class="ls-l" style="top:440px;left:820px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:bottom;durationin:1400;delayin:2200;">
            <a href="#" class="btn-grad-2">Check up now !</a>
          </div>
        </div>
        <div class="ls-slide" data-ls="duration:8000;transition2d:5;">
          <img class="ls-bg" src="img/bg-1.jpg" alt="background">
          <img class="ls-l" style="top:0;left:0;white-space:nowrap;" data-ls="offsetxin:left;offsetyin:0;durationin:1600;delayin:200;" src="img/hslider-6.png" alt="">	
          <img class="ls-l" style="top:0;left:500px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:0;durationin:1800;delayin:600;scaleyin:0%;transformoriginin:0% top 0;" src="img/hslider-3-1.png" alt="">
          <img class="ls-l" style="top:0;left:500px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:0;durationin:1800;delayin:600;scaleyin:0%;transformoriginin:0% bottom 0;" src="img/hslider-3-2.png" alt="">
          <img class="ls-l" style="top:435px;left:0px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:bottom;durationin:1400;delayin:1400;" src="img/hslider-7.png" alt="">
          <img class="ls-l" style="top:150px;left:450px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:-100;durationin:1400;delayin:1800;" src="img/hslider-8.png" alt="">
          <img class="ls-l" style="top:120px;left:255px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:-100;durationin:1400;delayin:2000;" src="img/hslider-9.png" alt="">
          <img class="ls-l" style="top:380px;left:25px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:0;durationin:1400;delayin:2200;scalein:.8;" src="img/hslider-10.png" alt="">
          <img class="ls-l" style="top:460px;left:360px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:0;durationin:1400;delayin:2300;" src="img/hslider-11.png" alt="">
          <div class="ls-l hslider-title" style="top:100px;left:820px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1400;delayin:1800;">
            <p>SANGOBION</p>
            <p>VITA-TONIK?</p>
          </div>
          <div class="ls-l hslider-desc" style="top:260px;left:820px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1400;delayin:2000;">
            <p>Jaga terus kesehatanmu, jangan sampai terkena</p>
            <p>5L. Ayo minum Sangobion selalu! Kini hadir dalam </p>
            <p>bentuk sirup.</p>
            <div class="space-2"></div>
            <p>Generasi Produktif, Generasi Bebas Anemia</p>
          </div>
          <div class="ls-l" style="top:410px;left:820px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:bottom;durationin:1400;delayin:2200;">
            <a href="#" class="btn-grad-2">Buy Now !</a>
          </div>
        </div>
  -->
      </div>
      <div id="layerslider_mobile" style="width:1366px;height:650px;margin:0 auto;" class="wow fadeIn d-block d-sm-none">
        <div class="ls-slide" data-ls="duration:8000;transition2d:5;">
          <img class="ls-bg" src="img/bg-1.jpg" alt="background">
          <img class="ls-l ls-background" style="top:0;left:-300px;right:0;white-space:nowrap;" src="img/hslider-2.png" alt="">	
          <img class="ls-l" style="top:0;left:200px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:0;durationin:1800;delayin:200;scaleyin:0%;transformoriginin:0% top 0;" src="img/hslider-3-1.png" alt="">
          <img class="ls-l" style="top:0;left:200px;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:0;durationin:1800;delayin:200;scaleyin:0%;transformoriginin:0% bottom 0;" src="img/hslider-3-2.png" alt="">
          <img class="ls-l" style="top:50px;left:20%;white-space:nowrap;height:600px;" src="img/hslider-1.png" alt="">
          <div class="ls-l hslider-title" style="top:10%;left:50%;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1400;delayin:600;">
            <p>The Blood</p>
            <p>Expert</p>
          </div>
          <div class="ls-l hslider-desc" style="top:40%;left:68%;white-space:initial;width: 50%;white-space: wrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1400;delayin:600;">
            <p>Dulu aktivitas saya sering terganggu karena kurang darah (anemia)<br>Sekarang saya selalu segar bersemangat</p>
            <p><strong>Mau tau Tips saya?</strong></p>
          </div>
          <div class="ls-l" style="top:77%;left:46%;white-space:nowrap;" data-ls="offsetxin:0;offsetyin:bottom;durationin:1400;delayin:600;">
            <a href="#" class="btn btn-white">Read More</a>
          </div>
        </div>
      </div>
    </section>
    <section class="section with-wavebackground">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-xl-10 col-lg-11 col-12">
            <div class="intro text-center">
              <div class="title wow fadeInDown">
                Benefits of iron to overall health
              </div>
              <div class="desc wow fadeInUp" data-wow-delay="0.5s">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla non arcu vitae dolor pretium malesuada eget vitae justo.<br>Praesent molestie libero ac finibus euismod.</p>
              </div>
            </div>
          </div>
          <div class="col-12">
            <div class="card-multicolor">
              <div class="row">
                <div class="col-lg-3 col-sm-6 col-12 card-col wow fadeInUp">
                  <div class="card-item">
                    <div class="image">
                      <img src="img/multicolor_1.png">
                    </div>
                    <div class="text">
                      <div class="title">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae porttitor erat.
                      </div>
                      <div class="type">
                        Work Productivity
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-12 card-col wow fadeInUp">
                  <div class="card-item">
                    <div class="image">
                      <img src="img/multicolor_2.png">
                    </div>
                    <div class="text">
                      <div class="title">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae porttitor erat.
                      </div>
                      <div class="type">
                        Cognition
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-12 card-col wow fadeInUp">
                  <div class="card-item">
                    <div class="image">
                      <img src="img/multicolor_3.png">
                    </div>
                    <div class="text">
                      <div class="title">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae porttitor erat.
                      </div>
                      <div class="type">
                        Growth
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-12 card-col wow fadeInUp">
                  <div class="card-item">
                    <div class="image">
                      <img src="img/multicolor_1.png">
                    </div>
                    <div class="text">
                      <div class="title">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae porttitor erat.
                      </div>
                      <div class="type">
                        Immune
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-10 col-lg-11 col-12">
            <div class="intro color-red text-center" style="padding-bottom: 10rem;">
              <div class="title wow fadeInDown">
                Why Iron is essential for blood health?
              </div>
              <div class="desc wow fadeInUp" data-wow-delay="0.5s">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
              </div>
              <div class="link">
                <a href="" class="btn btn-red">Read More</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="section slider-overflow">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section-heading wow fadeInDown">
              <div class="heading">
                Supports Optimum Blood Health
              </div>
            </div>
            <div class="filter-slider">
              <div class="owl-filter">
                <div class="spinner-left">
                  <div class="spinner">
                    <div class="dot1"></div>
                    <div class="dot2"></div>
                  </div>
                </div>
                <div class="spinner-right">
                  <div class="spinner">
                    <div class="dot1"></div>
                    <div class="dot2"></div>
                  </div>
                </div>
                <div class="owl-head wow fadeInUp" data-id="2">
                  <div class="category-wrapper">
                    <div class="filter-menu">
                      <ul class="list-inline">
                        <li class="list-inline-item filter-btn" data-filter=".children">Children</li>
                        <li class="list-inline-item filter-btn" data-filter=".adults">Adults</li>
                        <li class="list-inline-item filter-btn" data-filter=".pregnancy">Pregnancy</li>
                      </ul>
                    </div>
                  </div>
                  <div class="viewall-wrapper">
                    <div class="label">Find the right products for your needs</div>
                    <div class="viewall-link d-none"><a href="#">View All</a></div>
                  </div>
                </div>
                <div class="filter-item wow fadeIn" data-id="3">
                  <div class="item children wow fadeInUp">					
                    <div class="image">
                      <img src="img/img-5.png" alt="sangobion fit">
                    </div>
                    <div class="text">
                      <div class="title">Sangobion Fit</div>
                      <div class="desc"><p>Sangobion Fit is a food supplement that can fulfill various vitamins and iron to maintain your stamina and vitality.</p></div>
                    </div>
                    <a href="#" class="overlay">						
                      <div class="btn-learn-more">Learn More</div>
                      <div class="icon-arrow-down"></div>						
                    </a>
                  </div>
                  <div class="item adults wow fadeInUp">				
                    <div class="image">
                      <img src="img/img-6.png" alt="sangobion fit">                    
                    </div>
                    <div class="text">
                      <div class="title">Sangobion Femine</div>
                      <div class="desc"><p>Sangobion Femine MenstruPain has traditionally been able to help relieves menstrual pain. Reduces discomfort during menstruation</p></div>
                    </div>
                    <a href="#" class="overlay">						
                      <div class="btn-learn-more">Learn More</div>
                      <div class="icon-arrow-down"></div>						
                    </a>
                  </div>
                  <div class="item pregnancy wow fadeInUp">					
                    <div class="image">
                      <img src="img/img-5.png" alt="sangobion fit">
                    </div>
                    <div class="text">
                      <div class="title">Sangobion Fit</div>
                      <div class="desc"><p>Sangobion Fit is a food supplement that can fulfill various vitamins and iron to maintain your stamina and vitality.</p></div>
                    </div>
                    <a href="#" class="overlay">						
                      <div class="btn-learn-more">Learn More</div>
                      <div class="icon-arrow-down"></div>						
                    </a>
                  </div>
                  <div class="item children wow fadeInUp">	
                    <div class="image">                      
                      <img src="img/img-6.png" alt="sangobion fit">
                    </div>
                    <div class="text">
                      <div class="title">Sangobion Femine</div>
                      <div class="desc"><p>Sangobion Femine MenstruPain has traditionally been able to help relieves menstrual pain. Reduces discomfort during menstruation</p></div>
                    </div>
                    <a href="#" class="overlay">						
                      <div class="btn-learn-more">Learn More</div>
                      <div class="icon-arrow-down"></div>						
                    </a>
                  </div>
                </div>			
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php include('footer.php');?>
  </main>
  <?php include('script.php');?>
</body>
</html>

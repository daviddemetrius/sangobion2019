<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <?php include('metatag.php');?>
  <title>Home | SANGOBION</title>
  <link rel="icon" href="img/favicon.ico">
  <?php include('stylesheet.php');?>
</head>

<body>
  <main class="main-wrap" id="ask-expert">
    <?php $page = 'blood-health-expert';include('header.php');?>
    <!-- body start -->
    <section class="section">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="main-tabs wow fadeInDown">
              <div class="item-tab">
                FAQ
              </div>
              <div class="item-tab">
                Q & A
              </div>
              <!-- <div class="item-tab d-none">
                Ask the Expert
              </div> -->
            </div>
            <div class="main-slider wow fadeInUp" data-wow-delay="0.25s">
              <div class="item-slider">
                <div class="item-inside faq">
                  <div class="row">
                    <div class="col-12">
                      <div class="tab-heading">
                        FAQ
                      </div>
                      <div class="accordion" id="accordionExample">
                        <div class="card">
                          <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                              <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Collapsible Group Item #1
                              </button>
                            </h5>
                          </div>

                          <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                          </div>
                        </div>
                        <div class="card">
                          <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Collapsible Group Item #2
                              </button>
                            </h5>
                          </div>
                          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">
                              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                          </div>
                        </div>
                        <div class="card">
                          <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Collapsible Group Item #3
                              </button>
                            </h5>
                          </div>
                          <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="tab-info">
                        <div class="text">
                          <p>Can’t find what you’re looking for? Check out other’s Q&A with our expert</p>
                        </div>
                        <div class="link">
                          <a href="contactus.php" data-slide="2" class="btn btn-red">Read More</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-12">

                    </div>
                  </div>
                </div>
              </div>
              <div class="item-slider">
                <div class="item-inside q&a">
                  <div class="row">
                    <div class="col-12">
                      <div class="tab-heading blood">
                        Ask the Expert
                      </div>
                      <div class="chat-wrapper">
                        <div class="chat-item sender">
                          <div class="name">
                            Br****
                          </div>
                          <div class="chat">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                          </div>
                        </div>
                        <div class="chat-item reciever">
                          <div class="name">
                            Dr.James
                          </div>
                          <div class="chat">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                          </div>
                        </div>
                        <div class="chat-item sender">
                          <div class="name">
                            Br****
                          </div>
                          <div class="chat">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                          </div>
                        </div>
                        <div class="chat-item reciever">
                          <div class="name">
                            Dr.James
                          </div>
                          <div class="chat">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                          </div>
                        </div>
                        <div class="chat-item sender">
                          <div class="name">
                            Br****
                          </div>
                          <div class="chat">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                          </div>
                        </div>
                        <div class="chat-item reciever">
                          <div class="name">
                            Dr.James
                          </div>
                          <div class="chat">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                          </div>
                        </div>
                        <div class="chat-item sender">
                          <div class="name">
                            Ch****
                          </div>
                          <div class="chat">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                          </div>
                        </div>
                        <div class="chat-item sender">
                          <div class="name">
                            De****
                          </div>
                          <div class="chat">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                          </div>
                        </div>
                      </div>
                    <div class="tab-form">    
                        <form>
                          <div class="row">
                            <div class="col-12">
                              <div class="form-title smaller">
                                Still can’t find what you’re looking for? Send your question to our Expert
                              </div>
                            </div>
                            <div class="col-lg-5 col-12">
                              <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" placeholder="email">
                              </div>
                            </div>
                            <div class="col-lg-7 col-12">
                              <div class="form-group">
                                <label>Question</label>
                                <input type="text" class="form-control" placeholder="Question">
                              </div>
                            </div>
                            <div class="col-12">  
                              <div class="form-group justify-content-end">
                                <button type="submit" class="btn btn-red">Submit</button>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- <div class="item-slider ask-expert d-none">
                <div class="item-inside">
                  <img src="img/askexpert-tabs-background.png" class="as-background">
                  <div class="row">
                    <div class="offset-lg-6 col-lg-6 col-12">
                      <div class="tab-heading">
                        Ask the Expert
                      </div>
                      <div class="form-title">
                        Send your question..
                      </div>
                      <form>
                        <div class="form-group">
                          <label>Email</label>
                          <input type="email" class="form-control" placeholder="email">
                        </div>
                        <div class="form-group">
                          <label>Question</label>
                          <textarea rows="4" class="form-control" placeholder="Question"></textarea>
                        </div>
                        <div class="form-group justify-content-center">
                          <button type="submit" class="btn btn-red">Submit</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php include('footer.php');?>
  </main>
  <?php include('script.php');?>
</body>
</html>
